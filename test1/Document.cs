﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intelligize.Services.Mapper.SF.SubFormType.Library
{
    public class Document
    {
        public string formType;
        public int formTypeId;
        public string CIK;
        public string fileName;
        public string PathRelative;
        public int masterTreeTypeID;
        public int documentId;
        public int[] subFormTypeID;
        public bool ishtml = false;
    }
}

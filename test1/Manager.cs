﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intelligize.Services.Mapper.SF.SubFormType.Library
{
    public class Manager
    {
        public void FormTypeClassification()
        {
            SubFormType classifier = new SubFormType();
            List<Document> documents = classifier.DocumentSelection();
            for (int index = 0; index < documents.Count; index++)
            {
                classifier.Classification(documents[index]);
                
            }
        }
    }
}

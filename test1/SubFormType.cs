﻿using Intelligize.Commons.CommonHandler;
using Intelligize.Services.Mapper.SF.SubFormType.Library.Commons;
using Intelligize.Services.Mapper.SF.SubFormType.Library.DBHandlers;
using Intelligize.Services.Mapper.SF.SubFormType.Library.Identifiers;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intelligize.Services.Mapper.SF.SubFormType.Library
{
    public class SubFormType
    {
        private DataTable masterTreeTypeTable = HelperUtility.GetMasterTreeTypeTable();
        public List<Document> DocumentSelection()
        {
            List<Document> documents = new List<Document>();
            foreach (DataRow document in DBHelper.ReadTable(ConfigurationManager.AppSettings["DocumentSelectionQuery"].ToString()).Rows)
            {
                Document documentInformation = new Document();
                documentInformation.CIK = document["company_cik"].ToString();
                documentInformation.documentId = Convert.ToInt32(document["pk_id"].ToString());
                documentInformation.fileName = document["file_name"].ToString();
                documentInformation.formType = document["filing_form_type"].ToString();
                documentInformation.formTypeId = Convert.ToInt32(document["fk_formtype_id"].ToString());
                documentInformation.masterTreeTypeID = Convert.ToInt32(document["fk_master_tree_type_id"].ToString());
                documentInformation.PathRelative = document["path_relative"].ToString();
                documents.Add(documentInformation);
            }

            return documents;
        }
        public void UpdateParsedStatus(int pk_id)
        {
            DBHelper.ExecuteNonQuery("update tbl_document set is_parsed_npc = 1 where pk_id= " + pk_id);
        }
        public void InsertSubFormTypes(int documentId, int[] subFormTypeID)
        {
            if (subFormTypeID.Length <= 0)
            {
                return;
            }
            MySqlCommand cmd = new MySqlCommand();
            string subFormTypeInsertionQuery = @"insert ignore into db_sf.tbl_document_sub_form_type values";
            for (int index = 0; index < subFormTypeID.Length; index++)
            {
                subFormTypeInsertionQuery += "(@primaryId,@formtypeId" + index + "),";
                cmd.Parameters.AddWithValue("@formtypeId" + index, subFormTypeID[index]);
            }
            cmd.Parameters.AddWithValue("@primaryId", documentId);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = subFormTypeInsertionQuery.Trim(',') + ";";
            DBHelper.ExecuteNonQuery(cmd);
        }
        public void UpdateFormType(Document data)
        {
            DBHelper.ExecuteNonQuery("update tbl_document set filing_form_type = '" + data.formType + "' , fk_formtype_id = " + data.formTypeId + "  where pk_id =" + data.documentId);
        }
        public void Classification(Document document)
        {
            if (document.masterTreeTypeID == 159)
            {
                try
                {
                    if (Path.GetExtension(document.fileName).ToLower().Equals(".htm") ||
                        Path.GetExtension(document.fileName).ToLower().Equals(".html"))
                    {
                        document.ishtml = true;
                    }

                    document.subFormTypeID = PosFormType.ExtractFormType(
                         text:
                             File.ReadAllText(ConfigurationManager.AppSettings["FilePath_Main"].TrimEnd('\\') + "\\" +
                                              document.PathRelative + "\\" + document.fileName),
                         isHtml: document.ishtml,
                         documentId: document.documentId);
                    InsertSubFormTypes(document.documentId, document.subFormTypeID);

                }
                catch (Exception ex)
                {
                    IntelligizeLog.LogException(ex, IntelligizeApplications.SF, IntelligizeService.SFMapperSubFormType,
                                    IntelligizeDomain.Mapper, document.documentId.ToString(), true);
                }
            }
            else if (document.masterTreeTypeID == 676 || document.masterTreeTypeID == 677)
            {

                try
                {
                    if (Path.GetExtension(document.fileName).ToLower().Equals(".htm") ||
                        Path.GetExtension(document.fileName).ToLower().Equals(".html"))
                    {
                        document.ishtml = true;
                    }

                    document.subFormTypeID = DrsParentFormType.ExtractFormType(
                           text:
                               File.ReadAllText(ConfigurationManager.AppSettings["FilePath_Main"].TrimEnd('\\') + "\\" +
                                                document.PathRelative + "\\" + document.fileName),
                           isHtml: document.ishtml,
                           documentId: document.documentId);
                    InsertSubFormTypes(document.documentId, document.subFormTypeID);
                }
                catch (Exception ex)
                {
                    IntelligizeLog.LogException(ex, IntelligizeApplications.SF, IntelligizeService.SFMapperSubFormType,
                                    IntelligizeDomain.Mapper, document.documentId.ToString(), true);
                }
            }
            else if (document.masterTreeTypeID == 399 || document.masterTreeTypeID == 543)
            {
                char[] fType = document.formType.Substring(0, 1).ToLower().ToCharArray();
                var fileBytes =
                    File.ReadAllBytes(ConfigurationManager.AppSettings["FilePath_Main"].TrimEnd('\\') + "\\" +
                                      document.PathRelative + "\\" + document.fileName);

                string mainFilingForm = CorrespUploadSubtype.detect_LetterSubType_ByteArray(
                   fType[0], true,
                   fileBytes,
                   document.fileName,
                    document.CIK);
                if (!mainFilingForm.Trim().Equals(string.Empty))
                {
                    if (!mainFilingForm.Trim().Equals("ERROR"))
                    {
                        document.formType = mainFilingForm;
                        document.formTypeId = ExtractFormTypeId(document.formType);
                        UpdateFormType(document);
                    }
                }
            }
            UpdateParsedStatus(document.documentId);
        }

        public int ExtractFormTypeId(string formType)
        {
            if (formType.ToLower().Trim().Equals("upload"))
            {
                return 740;
            }
            else if (formType.ToLower().Trim().Equals("corresp") || formType.ToLower().Trim().Equals("corresp (cover)"))
            {
                return 739;
            }
            else
            {
                foreach (DataRow row in masterTreeTypeTable.Rows)
                {
                    if (row["TreeType"].ToString().ToLower().Trim().Equals(formType.ToLower().Trim()))
                    {
                        return Convert.ToInt32(row["pk_id"]);
                    }
                }
            }
            return -1;
        }

    }
}
